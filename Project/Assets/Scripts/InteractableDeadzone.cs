﻿using UnityEngine;
using Valve.VR.InteractionSystem;

public class InteractableDeadzone : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        Interactable interactable = other.GetComponent<Interactable>() ?? other.GetComponentInParent<Interactable>();

        if (!interactable) return;

        Transform interactableTransform = interactable.transform;
        interactableTransform.position = interactable.InitialPosition;
        interactableTransform.rotation = interactable.InitialRotation;

        Rigidbody rigidbody = interactable.GetComponent<Rigidbody>();
        if (rigidbody)
        {
            rigidbody.velocity = Vector3.zero;
        }
    }
}