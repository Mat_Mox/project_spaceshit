﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class ProceduralGeneration : MonoBehaviour
{
    [SerializeField] private List<string> alerts;
    [SerializeField] private List<GameObject> interactibles;

    [SerializeField] private List<string> alertsWiki;
    [SerializeField] private List<GameObject> interactiblesWiki;


    private int currentAlertIndex = 0;

    private GameObject interractibleToActivate = null;

    private VisualAlertGenerator alertScreen;

    private TimerScreen timerScreen;

    [SerializeField] private AudioClip wpSound;
    [SerializeField] private AudioClip failSound;
    [SerializeField] private AudioClip newRoundSound;
    private AudioSource audioSource;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    // Start is called before the first frame update
    void Start()
    {
        alertScreen = FindObjectOfType<VisualAlertGenerator>();

        alerts = new List<string>();
        //
        alerts.Add("Depressurization of the area " + Random.Range(1, 21));
        alerts.Add("Flush the toilet");
        alerts.Add("Reactor " + Random.Range(1, 20) + " dysfunction");
        alerts.Add("Flooded logic anno-reactor " + Random.Range(1, 20));
        alerts.Add("Reversed flow reverser");
        alerts.Add("Faulty oxygen generator");
        alerts.Add("Windows update failed");
        alerts.Add("Connection desynchronization");
        alerts.Add("Access violation 0x00" + Random.Range(1000, 10000));
        alerts.Add("Error 404");
        alerts.Add("The virus database has been updated");
        //
        currentAlertIndex = -1;

        interactibles = new List<GameObject>(FindObjectOfType<Generator>().SpawnedInterractibles);
        ShuffleAlerts();
        ShuffleInteractibles();

        timerScreen = FindObjectOfType<TimerScreen>();

        SetNextScenario();

        alertsWiki = alerts;
        interactiblesWiki = interactibles;

        // Shuffling
        int randomIndex;
        for (int i = 0; i < alertsWiki.Count; i++)
        {
            string temp = alertsWiki[i];
            GameObject temp2 = interactiblesWiki[i];
            Random.InitState(i);
            randomIndex = Random.Range(i, alertsWiki.Count);
            alertsWiki[i] = alertsWiki[randomIndex];
            interactiblesWiki[i] = interactiblesWiki[randomIndex];
            alertsWiki[randomIndex] = temp;
            interactiblesWiki[randomIndex] = temp2;
        }

    }

    void ShuffleAlerts() // to shuffle (parameter list)
    {
        for (int i = 0; i < alerts.Count; i++)
        {
            string temp = alerts[i];
            int randomIndex = Random.Range(i, alerts.Count);
            alerts[i] = alerts[randomIndex];
            alerts[randomIndex] = temp;
        }
    }

    void ShuffleInteractibles()
    {
        for (int i = 0; i < interactibles.Count; i++)
        {
            GameObject temp = interactibles[i];
            int randomIndex = Random.Range(i, interactibles.Count);
            interactibles[i] = interactibles[randomIndex];
            interactibles[randomIndex] = temp;
        }
    }

    public void SetNextScenario()
    {
        currentAlertIndex += 1;

        if (currentAlertIndex >= alerts.Count)
        {
            ShuffleAlerts();
            ShuffleInteractibles();
            currentAlertIndex = 0;
        }

        interractibleToActivate = interactibles[currentAlertIndex];
        

        audioSource.PlayOneShot(newRoundSound);
        alertScreen.GenerateVisualAlert(alerts[currentAlertIndex]);
        timerScreen.NewRound();
    }

    public void CheckScenario(GameObject usedInteractible)
    {
        Debug.Log(usedInteractible.name + " - " + interactibles[currentAlertIndex].name);
        if (usedInteractible != interactibles[currentAlertIndex])
        {
            Debug.Log("Mauvais");
            // timer -1/4
            timerScreen.OnError();
            audioSource.PlayOneShot(failSound);

            return;
        }

        audioSource.PlayOneShot(wpSound);
        SetNextScenario();
    }

    private void Update()
    {
        Text theText = GameObject.Find("Log")?.GetComponent<Text>();
        if (!theText) return;
        theText.text = "";

        for (int i = 0; i < alerts.Count; ++i)
        {
            theText.text += alerts[i];
            theText.text += " - ";
            theText.text += interactibles[i].GetComponent<InteractibleData>().data.colorName;
            theText.text += " ";
            theText.text += interactibles[i].GetComponent<InteractibleData>().name;
            theText.text += "\n\n";
        }
    }

}