﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IrisScanner : MonoBehaviour
{
    private ProceduralGeneration pgen;

    private Color initialColor;
    [SerializeField] private Color scanColor = Color.green;

    private void Start()
    {
        pgen = FindObjectOfType<ProceduralGeneration>();
        initialColor = ren.material.color;
    }

    [SerializeField] Renderer ren;
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Face"))
        {
            ren.material.SetColor("_Color", scanColor);
            pgen.CheckScenario(gameObject);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Face"))
        {
            ren.material.SetColor("_Color", initialColor);
        }
    }

}
