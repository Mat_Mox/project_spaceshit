﻿//======= Copyright (c) Valve Corporation, All rights reserved. ===============

using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using Valve.VR.InteractionSystem;

public class MenuButtons : MonoBehaviour
{

    enum Action
    {
        Play, Quit
    }


    [SerializeField] Action action;


    public void OnButtonDown(Hand fromHand)
    {
        if (action == Action.Play)
        {
            SceneManager.LoadScene("MainScene");
            Debug.Log("play");

        }

        if (action == Action.Quit)
        {
            Application.Quit();
            Debug.Log("quit");
        }
        
    }






}