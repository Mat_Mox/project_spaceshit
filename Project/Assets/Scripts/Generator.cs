﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class Generator : MonoBehaviour
{
    [Serializable]
    public struct ColorAndNames
    {
        public Color color;
        public string colorName;
    }

    List<Transform> spawnPoints;
    [SerializeField] List<GameObject> interactibles;
    [SerializeField] GameObject battery;

    List<GameObject> spawnedInterractibles = new List<GameObject>();

    public List<GameObject> SpawnedInterractibles => spawnedInterractibles;

    [SerializeField] private List<ColorAndNames> colorsAndNames = new List<ColorAndNames>
    {
        new ColorAndNames
        {
            color = Color.yellow,
            colorName = "Yellow",
        },
        new ColorAndNames
        {
            color = Color.red,
            colorName = "Red",
        },
        new ColorAndNames
        {
            color = Color.green,
            colorName = "Green",
        },
        new ColorAndNames
        {
            color = Color.blue,
            colorName = "Blue",
        },
        new ColorAndNames
        {
            color = Color.magenta,
            colorName = "Magenta",
        },
    };

    private readonly Dictionary<string, int> maxSpawns = new Dictionary<string, int>
    {
        {"BatteryBase", 5},
        {"Button", 5},
        {"CircularDrive", 5},
        {"Iris_Scanner", 5},
        {"Lever", 5},
    };

    private readonly Dictionary<string, Queue<ColorAndNames>> colorsQueues = new Dictionary<string, Queue<ColorAndNames>>();

    // Start is called before the first frame update
    void Awake()
    {
        foreach (string interactibleName in maxSpawns.Keys)
        {
            colorsQueues.Add(interactibleName, new Queue<ColorAndNames>());
            InitColorQueue(interactibleName);
        }

        spawnPoints = new List<Transform>();

        foreach (Transform child in transform)
        {
            spawnPoints.Add(child.transform);
        }

        SpawnInteractibles();
    }

    private void SpawnInteractibles()
    {
        if (interactibles.Count == 0 || interactibles.Count > 20) return;

        Dictionary<string, int> spawnsCount = new Dictionary<string, int>();
        foreach (string objName in maxSpawns.Keys)
        {
            spawnsCount.Add(objName, 0);
        }

        foreach (Transform child in spawnPoints)
        {
            bool spawned = false;

            while (!spawned)
            {
                int rand = Random.Range(0, interactibles.Count);
                GameObject obj = interactibles[rand];

                foreach (KeyValuePair<string, int> pair in spawnsCount)
                {
                    if (obj.name == pair.Key && pair.Value < maxSpawns[pair.Key])
                    {
                        ++spawnsCount[pair.Key];

                        GameObject interactibleClone =
                            Instantiate(interactibles[rand], child.position, child.rotation);
                        spawnedInterractibles.Add(interactibleClone);

                        ColorAndNames cn = colorsQueues[pair.Key].Dequeue();
                        interactibleClone.GetComponent<InteractibleData>().Initialize(cn);

                        if (pair.Key == "BatteryBase")
                        {
                            InteractibleData interactibleData = Instantiate(battery).GetComponent<InteractibleData>();
                            interactibleData.Initialize(cn);
                        }

                        spawned = true;
                        break;
                    }
                }
            }
        }
    }


    void InitColorQueue(string interactibleName)
    {
        // Shuffling
        for (int i = 0; i < colorsAndNames.Count; i++)
        {
            ColorAndNames temp = colorsAndNames[i];
            int randomIndex = Random.Range(i, colorsAndNames.Count);
            colorsAndNames[i] = colorsAndNames[randomIndex];
            colorsAndNames[randomIndex] = temp;
        }

        foreach (ColorAndNames var in colorsAndNames)
        {
            colorsQueues[interactibleName].Enqueue(var);
        }
    }
}