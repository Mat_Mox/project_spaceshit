﻿using Valve.VR.InteractionSystem;

public class CustomThrowable : Throwable
{
    protected override void OnAttachedToHand(Hand hand)
    {
        if (enabled) base.OnAttachedToHand(hand);
    }

    protected override void OnHandHoverBegin(Hand hand)
    {
        if (enabled) base.OnHandHoverBegin(hand);
    }

    protected override void HandHoverUpdate(Hand hand)
    {
        if (enabled) base.HandHoverUpdate(hand);
    }
}